
-- income
CREATE TABLE "income" (
  "id" UUID PRIMARY KEY,
  "increment_id" VARCHAR(10),
  "branch_id" UUID,
  "supplier_id" UUID,
  "date_time" DATE,
  "status" VARCHAR(20), 
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

-- income_product
CREATE TABLE "income_product" (
  "id" UUID PRIMARY KEY,
  "income_id" UUID NOT NULL REFERENCES "income"("id"),
  "brand_id" UUID, 
  "category_id" UUID,
  "product_id" UUID,
  "barcode" VARCHAR(100),
  "quantity" BIGINT,
  "income_price" NUMERIC,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

-- remainder
CREATE TABLE "remainder" (
  "id" UUID PRIMARY KEY,
  "brand_id" UUID,
  "branch_id" UUID,
  "category_id" UUID,
  "product_id" UUID,
  "barcode" VARCHAR(100),
  "income_price" NUMERIC,
  "quantity" BIGINT, 
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);
