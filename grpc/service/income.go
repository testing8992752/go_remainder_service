package service

import (
	"context"
	"remainder_service/config"
	"remainder_service/genproto/remainder_service"
	"remainder_service/grpc/client"
	"remainder_service/packages/logger"
	"remainder_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type IncomeService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*remainder_service.UnimplementedIncomeServiceServer
}

func NewIncomeService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *IncomeService {
	return &IncomeService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *IncomeService) Create(ctx context.Context, req *remainder_service.CreateIncome) (resp *remainder_service.Income, err error) {

	i.log.Info("---CreateIncome------>", logger.Any("req", req))

	pKey, err := i.strg.Income().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateIncome->Income->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Income().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyIncome->Income->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *IncomeService) GetByID(ctx context.Context, req *remainder_service.IncomePrimaryKey) (resp *remainder_service.Income, err error) {

	i.log.Info("---GetIncomeByID------>", logger.Any("req", req))

	resp, err = i.strg.Income().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetIncomeByID->Income->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *IncomeService) GetList(ctx context.Context, req *remainder_service.GetListIncomeRequest) (resp *remainder_service.GetListIncomeResponse, err error) {

	i.log.Info("---GetIncomes------>", logger.Any("req", req))

	resp, err = i.strg.Income().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetIncomes->Income->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *IncomeService) Update(ctx context.Context, req *remainder_service.UpdateIncome) (resp *remainder_service.Income, err error) {

	i.log.Info("---UpdateIncome------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Income().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateIncome--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Income().GetByPKey(ctx, &remainder_service.IncomePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetIncome->Income->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *IncomeService) Delete(ctx context.Context, req *remainder_service.IncomePrimaryKey) (resp *remainder_service.Empty, err error) {

	i.log.Info("---DeleteIncome------>", logger.Any("req", req))

	err = i.strg.Income().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteIncome->Income->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &remainder_service.Empty{}, nil
}
