package service

import (
	"context"
	"remainder_service/config"
	"remainder_service/genproto/remainder_service"
	"remainder_service/grpc/client"
	"remainder_service/packages/logger"
	"remainder_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type IncomeProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*remainder_service.UnimplementedIncomeProductServiceServer
}

func NewIncomeProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *IncomeProductService {
	return &IncomeProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *IncomeProductService) Create(ctx context.Context, req *remainder_service.CreateIncomeProduct) (resp *remainder_service.IncomeProduct, err error) {

	i.log.Info("---CreateIncomeProduct------>", logger.Any("req", req))

	pKey, err := i.strg.IncomeProduct().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateIncomeProduct->IncomeProduct->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.IncomeProduct().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyIncomeProduct->IncomeProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *IncomeProductService) GetByID(ctx context.Context, req *remainder_service.IncomeProductPrimaryKey) (resp *remainder_service.IncomeProduct, err error) {

	i.log.Info("---GetIncomeProductByID------>", logger.Any("req", req))

	resp, err = i.strg.IncomeProduct().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetIncomeProductByID->IncomeProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *IncomeProductService) GetList(ctx context.Context, req *remainder_service.GetListIncomeProductRequest) (resp *remainder_service.GetListIncomeProductResponse, err error) {

	i.log.Info("---GetIncomeProducts------>", logger.Any("req", req))

	resp, err = i.strg.IncomeProduct().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetIncomeProducts->IncomeProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *IncomeProductService) Update(ctx context.Context, req *remainder_service.UpdateIncomeProduct) (resp *remainder_service.IncomeProduct, err error) {

	i.log.Info("---UpdateIncomeProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.IncomeProduct().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateIncomeProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.IncomeProduct().GetByPKey(ctx, &remainder_service.IncomeProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetIncomeProduct->IncomeProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *IncomeProductService) Delete(ctx context.Context, req *remainder_service.IncomeProductPrimaryKey) (resp *remainder_service.Empty, err error) {

	i.log.Info("---DeleteIncomeProduct------>", logger.Any("req", req))

	err = i.strg.IncomeProduct().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteIncomeProduct->IncomeProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &remainder_service.Empty{}, nil
}
