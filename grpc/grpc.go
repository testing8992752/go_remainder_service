package grpc

import (
	"remainder_service/config"
	"remainder_service/genproto/remainder_service"
	"remainder_service/grpc/client"
	"remainder_service/grpc/service"
	"remainder_service/packages/logger"
	"remainder_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	remainder_service.RegisterIncomeServiceServer(grpcServer, service.NewIncomeService(cfg, log, strg, srvc))
	remainder_service.RegisterIncomeProductServiceServer(grpcServer, service.NewIncomeProductService(cfg, log, strg, srvc))
	remainder_service.RegisterRemainderServiceServer(grpcServer, service.NewRemainderService(cfg, log, strg, srvc))
	reflection.Register(grpcServer)
	return
}
