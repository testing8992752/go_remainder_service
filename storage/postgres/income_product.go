package memory

import (
	"context"
	"database/sql"
	"remainder_service/genproto/remainder_service"
	"remainder_service/packages/helper"
	"remainder_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type IncomeProductRepo struct {
	db *pgxpool.Pool
}

func NewIncomeProductRepo(db *pgxpool.Pool) storage.IncomeProductRepoI {
	return &IncomeProductRepo{
		db: db,
	}
}

func (c *IncomeProductRepo) Create(ctx context.Context, req *remainder_service.CreateIncomeProduct) (resp *remainder_service.IncomeProductPrimaryKey, err error) {

	var id = uuid.New().String()

	query := `
		INSERT INTO "income_product" (
			id,
			income_id,
			brand_id,
			category_id,
			product_id,
			barcode,
		    quantity,
			income_price,
			updated_at) VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.IncomeId,
		req.Brand,
		req.CategoryId,
		req.ProductId,
		req.Barcode,
		req.Quantity,
		req.IncomePrice,
	)

	if err != nil {
		return nil, err
	}

	return &remainder_service.IncomeProductPrimaryKey{Id: id}, nil
}

func (c *IncomeProductRepo) GetByPKey(ctx context.Context, req *remainder_service.IncomeProductPrimaryKey) (resp *remainder_service.IncomeProduct, err error) {

	query := `
		SELECT
			id,
			income_id,
			brand_id,
			category_id,
			product_id,
			barcode,
		    quantity,
			income_price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "income_product"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		incomeId    sql.NullString
		brandId     sql.NullString
		categoryId  sql.NullString
		productId   sql.NullString
		barcode     sql.NullString
		quantity    sql.NullInt64
		incomePrice sql.NullFloat64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&incomeId,
		&brandId,
		&categoryId,
		&productId,
		&barcode,
		&quantity,
		&incomePrice,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &remainder_service.IncomeProduct{
		Id:          id.String,
		IncomeId:    incomeId.String,
		Brand:       brandId.String,
		CategoryId:  categoryId.String,
		ProductId:   productId.String,
		Barcode:     barcode.String,
		Quantity:    quantity.Int64,
		IncomePrice: float32(incomePrice.Float64),
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *IncomeProductRepo) GetAll(ctx context.Context, req *remainder_service.GetListIncomeProductRequest) (resp *remainder_service.GetListIncomeProductResponse, err error) {

	resp = &remainder_service.GetListIncomeProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			income_id,
			brand_id,
			category_id,
			product_id,
			barcode,
		    quantity,
			income_price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "income_product"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			incomeId    sql.NullString
			brandId     sql.NullString
			categoryId  sql.NullString
			productId   sql.NullString
			barcode     sql.NullString
			quantity    sql.NullInt64
			incomePrice sql.NullFloat64
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&incomeId,
			&brandId,
			&categoryId,
			&productId,
			&barcode,
			&quantity,
			&incomePrice,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.IncomeProducts = append(resp.IncomeProducts, &remainder_service.IncomeProduct{
			Id:          id.String,
			IncomeId:    incomeId.String,
			Brand:       brandId.String,
			CategoryId:  categoryId.String,
			ProductId:   productId.String,
			Barcode:     barcode.String,
			Quantity:    quantity.Int64,
			IncomePrice: float32(incomePrice.Float64),
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *IncomeProductRepo) Update(ctx context.Context, req *remainder_service.UpdateIncomeProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "income_product"
			SET
				id = :id,
				income_id =: income_id,
				brand_id = :brand_id,
				category_id = :category_id,
				product_id = :product_id,
				barcode = :barcode,
				quantity = :quantity,
				income_price = :income_price,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"income_id":    req.GetIncomeId(),
		"brand_id":     req.GetBrand(),
		"category_id":  req.GetCategoryId(),
		"product_id":   req.GetProductId(),
		"barcode":      req.GetBarcode(),
		"quantity":     req.GetQuantity(),
		"income_price": req.GetIncomePrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *IncomeProductRepo) Delete(ctx context.Context, req *remainder_service.IncomeProductPrimaryKey) error {

	query := `DELETE FROM "income_product" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
