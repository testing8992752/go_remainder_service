package memory

import (
	"context"
	"database/sql"
	"remainder_service/genproto/remainder_service"
	"remainder_service/packages/helper"
	"remainder_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type RemainderRepo struct {
	db *pgxpool.Pool
}

func NewRemainderRepo(db *pgxpool.Pool) storage.RemainderRepoI {
	return &RemainderRepo{
		db: db,
	}
}

func (c *RemainderRepo) Create(ctx context.Context, req *remainder_service.CreateRemainder) (resp *remainder_service.RemainderPrimaryKey, err error) {

	var id = uuid.New().String()

	query := `
		INSERT INTO "remainder" (
			id,
			branch_id,
			brand_id,
			category_id,
			product_id,
			barcode,
		    income_price,
		    quantity,
			updated_at) VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.Branch,
		req.Brand,
		req.CategoryId,
		req.ProductId,
		req.Barcode,
		req.IncomePrice,
		req.Quantity,
	)

	if err != nil {
		return nil, err
	}

	return &remainder_service.RemainderPrimaryKey{Id: id}, nil
}

func (c *RemainderRepo) GetByPKey(ctx context.Context, req *remainder_service.RemainderPrimaryKey) (resp *remainder_service.Remainder, err error) {

	query := `
		SELECT
			id,
			branch_id,
			brand_id,
			category_id,
			product_id,
			barcode,
		    income_price,
		    quantity,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "remainder"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		branchId    sql.NullString
		brandId     sql.NullString
		categoryId  sql.NullString
		productId   sql.NullString
		barcode     sql.NullString
		incomePrice sql.NullFloat64
		quantity    sql.NullInt64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branchId,
		&brandId,
		&categoryId,
		&productId,
		&barcode,
		&incomePrice,
		&quantity,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &remainder_service.Remainder{
		Id:          id.String,
		Brand:       branchId.String,
		Branch:      brandId.String,
		CategoryId:  categoryId.String,
		ProductId:   productId.String,
		Barcode:     barcode.String,
		IncomePrice: float32(incomePrice.Float64),
		Quantity:    quantity.Int64,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *RemainderRepo) GetAll(ctx context.Context, req *remainder_service.GetListRemainderRequest) (resp *remainder_service.GetListRemainderResponse, err error) {

	resp = &remainder_service.GetListRemainderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			brand_id,
			category_id,
			product_id,
			barcode,
		    income_price,
		    quantity,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "remainder"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			branchId    sql.NullString
			brandId     sql.NullString
			categoryId  sql.NullString
			productId   sql.NullString
			barcode     sql.NullString
			incomePrice sql.NullFloat64
			quantity    sql.NullInt64
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branchId,
			&brandId,
			&categoryId,
			&productId,
			&barcode,
			&incomePrice,
			&quantity,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Remainders = append(resp.Remainders, &remainder_service.Remainder{
			Id:          id.String,
			Brand:       brandId.String,
			Branch:      branchId.String,
			CategoryId:  categoryId.String,
			ProductId:   productId.String,
			Barcode:     barcode.String,
			IncomePrice: float32(incomePrice.Float64),
			Quantity:    quantity.Int64,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *RemainderRepo) Update(ctx context.Context, req *remainder_service.UpdateRemainder) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "remainder"
			SET
				id = :id,
				branch_id = :branch_id,
				brand_id = :brand_id,
				category_id = :category_id,
				product_id = :product_id,
				barcode = :barcode,
				income_price = :income_price,
				quantity = :quantity,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"branch_id":    req.GetBranch(),
		"brand_id":     req.GetBrand(),
		"category_id":  req.GetCategoryId(),
		"product_id":   req.GetProductId(),
		"barcode":      req.GetBarcode(),
		"income_price": req.GetIncomePrice(),
		"quantity":     req.GetQuantity(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *RemainderRepo) Delete(ctx context.Context, req *remainder_service.RemainderPrimaryKey) error {

	query := `DELETE FROM "remainder" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
