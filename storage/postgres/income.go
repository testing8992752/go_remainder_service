package memory

import (
	"context"
	"database/sql"
	"remainder_service/genproto/remainder_service"
	"remainder_service/packages/helper"
	"remainder_service/storage"
	"strconv"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type IncomeRepo struct {
	db *pgxpool.Pool
}

func NewIncomeRepo(db *pgxpool.Pool) storage.IncomeRepoI {
	return &IncomeRepo{
		db: db,
	}
}

func (c *IncomeRepo) Create(ctx context.Context, req *remainder_service.CreateIncome) (resp *remainder_service.IncomePrimaryKey, err error) {

	var (
		id           = uuid.New().String()
		increment_id = ""
	)

	max_query := `
		SELECT MAX(increment_id) 
			FROM "income"
		`
	err = c.db.QueryRow(ctx, max_query).Scan(&increment_id)
	if err != nil {
		if err.Error() != "can't scan into dest[0]: cannot scan null into *string" {
			return resp, err
		} else {
			increment_id = "P-0000000"
		}
	}

	digit, _ := strconv.Atoi(increment_id[2:])

	query := `
		INSERT INTO "income" (
			id,
			increment_id,
			branch_id,
			supplier_id,
			date_time,
			status,
			updated_at) VALUES
			($1, $2, $3, $4, TO_DATE($5, 'YYYY-MM-DD'), $6, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		"P-"+helper.GetSerialId(digit),
		req.BranchId,
		req.SupplierId,
		req.Date,
		"new",
	)

	if err != nil {
		return nil, err
	}

	return &remainder_service.IncomePrimaryKey{Id: id}, nil
}

func (c *IncomeRepo) GetByPKey(ctx context.Context, req *remainder_service.IncomePrimaryKey) (resp *remainder_service.Income, err error) {

	query := `
		SELECT
			id,
			increment_id,
			branch_id,
			supplier_id,
			TO_CHAR(date_time, 'YYYY-MM-DD HH24:MI:SS'),
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "income"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		incrementId sql.NullString
		branchId    sql.NullString
		supplierId  sql.NullString
		date        sql.NullString
		status      sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&incrementId,
		&branchId,
		&supplierId,
		&date,
		&status,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &remainder_service.Income{
		Id:          id.String,
		IncomeIncId: incrementId.String,
		BranchId:    branchId.String,
		SupplierId:  supplierId.String,
		Date:        date.String,
		Status:      status.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *IncomeRepo) GetAll(ctx context.Context, req *remainder_service.GetListIncomeRequest) (resp *remainder_service.GetListIncomeResponse, err error) {

	resp = &remainder_service.GetListIncomeResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			increment_id,
			branch_id,
			supplier_id,
			TO_CHAR(date_time, 'YYYY-MM-DD HH24:MI:SS'),
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "income"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			incrementId sql.NullString
			branchId    sql.NullString
			supplierId  sql.NullString
			date        sql.NullString
			status      sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&incrementId,
			&branchId,
			&supplierId,
			&date,
			&status,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Incomes = append(resp.Incomes, &remainder_service.Income{
			Id:          id.String,
			IncomeIncId: incrementId.String,
			BranchId:    branchId.String,
			SupplierId:  supplierId.String,
			Date:        date.String,
			Status:      status.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *IncomeRepo) Update(ctx context.Context, req *remainder_service.UpdateIncome) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "income"
			SET
				id = :id,
				branch_id = :branch_id,
				supplier_id = :supplier_id,
				date_time = :date,
				status = :status,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"branch_id":   req.GetBranchId(),
		"supplier_id": req.GetSupplierId(),
		"date":        req.GetDate(),
		"status":      req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *IncomeRepo) Delete(ctx context.Context, req *remainder_service.IncomePrimaryKey) error {

	query := `DELETE FROM "income" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
