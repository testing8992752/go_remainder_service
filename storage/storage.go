package storage

import (
	"context"
	"remainder_service/genproto/remainder_service"
)

type StorageI interface {
	CloseDB()
	Income() IncomeRepoI
	IncomeProduct() IncomeProductRepoI
	Remainder() RemainderRepoI
}

type IncomeRepoI interface {
	Create(ctx context.Context, req *remainder_service.CreateIncome) (resp *remainder_service.IncomePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *remainder_service.IncomePrimaryKey) (resp *remainder_service.Income, err error)
	GetAll(ctx context.Context, req *remainder_service.GetListIncomeRequest) (resp *remainder_service.GetListIncomeResponse, err error)
	Update(ctx context.Context, req *remainder_service.UpdateIncome) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *remainder_service.IncomePrimaryKey) error
}

type RemainderRepoI interface {
	Create(ctx context.Context, req *remainder_service.CreateRemainder) (resp *remainder_service.RemainderPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *remainder_service.RemainderPrimaryKey) (resp *remainder_service.Remainder, err error)
	GetAll(ctx context.Context, req *remainder_service.GetListRemainderRequest) (resp *remainder_service.GetListRemainderResponse, err error)
	Update(ctx context.Context, req *remainder_service.UpdateRemainder) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *remainder_service.RemainderPrimaryKey) error
}

type IncomeProductRepoI interface {
	Create(ctx context.Context, req *remainder_service.CreateIncomeProduct) (resp *remainder_service.IncomeProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *remainder_service.IncomeProductPrimaryKey) (resp *remainder_service.IncomeProduct, err error)
	GetAll(ctx context.Context, req *remainder_service.GetListIncomeProductRequest) (resp *remainder_service.GetListIncomeProductResponse, err error)
	Update(ctx context.Context, req *remainder_service.UpdateIncomeProduct) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *remainder_service.IncomeProductPrimaryKey) error
}
