CURRENT_DIR=$(shell pwd)

APP=$(shell basename "${CURRENT_DIR}")
APP_CMD_DIR=${CURRENT_DIR}/cmd

TAG=latest
ENV_TAG=latest

proto-gen:
	./scripts/gen-proto.sh  ${CURRENT_DIR}
	sudo rm -rf ${GOROOT}/src/genproto

pull-proto-module:
	git submodule update --init --recursive

update-proto-module:
	git submodule update --remote --merge

migration-up:
	migrate -path ./migrations/postgres -database 'postgres://zafar:2605@0.0.0.0:5432/remainder_service?sslmode=disable' up

migration-down:
	migrate -path ./migrations -database 'postgres://zafar:2605@0.0.0.0:5432/remainder_service?sslmode=disable' down

swag_init:
	swag init -g api/main.go -o api/docs

run:
	go run cmd/main.go